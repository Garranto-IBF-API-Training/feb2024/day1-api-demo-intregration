package com.garranto;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import com.google.gson.JsonArray;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.io.IOException;

public class CountryApp {
    public static void main(String[] args) {
        System.out.println("Country App");
        System.out.println("=================");

        OkHttpClient client = new OkHttpClient().newBuilder().build();

        Gson gson = new Gson();

        Request request = new Request.Builder()
                .url("https://restcountries.com/v3.1/all")
                .method("GET",null)
                .build();

        try {
            Response response = client.newCall(request).execute();
            String resourceString = response.body().string();
            JsonArray countriesArray = gson.fromJson(resourceString, JsonArray.class);

            System.out.println("Total Countries: "+countriesArray.size());
            for(JsonElement countryElement :countriesArray){

                JsonObject country = countryElement.getAsJsonObject();
                JsonObject name = country.getAsJsonObject("name");
                JsonPrimitive officialName = name.getAsJsonPrimitive("official");
                System.out.println(officialName);
            }
        } catch (IOException e) {
            System.out.println("Server Request Failed");
        }

    }
}


//jsonelement
//array ---  jsonarray
//object --- jsonobject
//primitve ---  jsonprimitive

//1. send request to country rest (okhttp)
//2. get the result and convert the text to appropriate object for processing
//3. iterate the array, extract the name property of all the items in the array  (bson)
//4. print the name
//5 handle exceptions
