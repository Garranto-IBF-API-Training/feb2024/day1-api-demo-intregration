package com.garranto;

import com.google.gson.*;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.util.Scanner;

public class CurrencyConversionApp {
    public static void main(String[] args) {
        System.out.println("Currency Conversion");
        System.out.println("========================");

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the source currency:");
        String source = scanner.next();
        System.out.println("Enter the target currrency:");
        String target = scanner.next();
        System.out.println("Enter the amount to convert:");
        String amount = scanner.next();

        String apiKey = "GfRrzo6glVur4K5kJAsQxGWhmAKi1U4x" ;

        OkHttpClient client = new OkHttpClient().newBuilder().build();

        Gson gson = new Gson();

        HttpUrl.Builder builder = HttpUrl.parse("https://api.apilayer.com/exchangerates_data/convert").newBuilder();
        builder.addQueryParameter("from",source);
        builder.addQueryParameter("to",target);
        builder.addQueryParameter("amount",amount);

        HttpUrl url = builder.build();

//        String url = "https://api.apilayer.com/exchangerates_data/convert?to="+target+"&from="+source+"&amount="+amount;

        Request request = new Request.Builder()
                .url(url)
                .method("GET",null)
                .addHeader("apiKey",apiKey)
                .build();

        try {
            Response response = client.newCall(request).execute();
            String resourceString = response.body().string();
            JsonObject result = gson.fromJson(resourceString,JsonObject.class);
            JsonPrimitive convertedValue = result.getAsJsonPrimitive("result");
            JsonObject info = result.getAsJsonObject("info");
            JsonPrimitive rate = info.getAsJsonPrimitive("rate");
            System.out.println("Converted Amount: "+convertedValue+" at the rate of "+rate+" per"+source);
//            System.out.println(convertedValue);

        } catch (IOException e) {
            System.out.println("Server Request Failed");
        }

//        get the input values from the user
//        build request with apikey header and add the input values in the url paramters
//        send the request, convert the result to suitable representation
//        extract the requyired result value
//        print it in the screen
    }
}
